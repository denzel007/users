{% extends "base.volt" %}
{% block content %}
    <form id="" action="" method="post">
        <div class="page_heading group_view_heading">
            <a href="{{ url('users') }}" class="back_btn"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
            <h1>{{ segment.title }}</h1>
            <div class="top_buttons one_type_buttons">
                <div class="btn-group massActions hide">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-list" aria-hidden="true"></i>
                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                    </button>
                    <div class="dropdown-menu screen_menu">
                        <a class="dropdown-item red_color" data-action="delete">Reset password</a>
                    </div>
                </div>
                <button type="button" class="btn btn-warning editsegment" data-id="{{ segment.id }}" data-toggle="modal" data-target="#editSegment" >EDIT SEGMENT</button>
            </div>
        </div>

        <div class="page_container group_full_view_page">
            <div class="top_select_block">
                <div class="pull-left">
                    <select class="selectpicker" name="changeSegment">
                        {% for segm in segmentsList %}
                            <option value="{{ url('users/segment/' ~ segm.id) }}" {{ segm.id == segment.id ? 'selected' : '' }} >{{ segm.title }}</option>
                        {% endfor %}
                    </select>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="segment_box segment_box_content">
                <div class="conditions_tags">
                    <label class="cond_title">Conditions:</label>

			        {% set dealConditions = segment.getDeals() %}
			        {% if dealConditions is not empty %}
			            {% for deal in dealConditions %}
			                <button type="button" class="btn btn-default">{{ deal.purchased == constant('SegmentsDealsRel::PURCHASED') ? 'Purchased' : 'Not purchased' }} {{ deal.Deals.Info.name }}</button>
			            {% endfor %}
			        {% endif %}

			        {% set stationConditions = segment.getSkiStations() %}
			        {% if stationConditions is not empty %}
		                {% for station in stationConditions %}
		                    <button type="button" class="btn btn-default">{{ station.visited == constant('SegmentsSkiStationsRel::VISITED') ? 'Visited' : 'Not visited' }} {{ station.Companies.Info.placeName }} on {{ station.SkiStations.name }}</button>
		                {% endfor %}
			        {% endif %}

                </div>
                <h3 class="sub_heading">USERS ({{ users.total_items }})</h3>
                <div class="page_container users_container">
                    <div class="table default_table users_table">
                        <div class="table_row table_heading">
                            <div class="table_cell">
                                <div class="checkbox_body">
                                    <div class="boxCheckbox checkAll">
                                        <div class="check check_all_users">
                                            <i class="fa fa-square-cube disabled_icon"></i>
                                            <i class="fa fa-check-square ACTIVE_icon"></i>
                                            <input id="" type="checkbox" class="actualCheckItem" name="" value=""/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="table_cell">USER NAME</div>
                            <div class="table_cell">EMAIL</div>
                            <div class="table_cell text-center">SEX</div>
                             <div class="table_cell text-center">DATE OF BIRTH</div>
                              <div class="table_cell">CITY</div>
                            <div class="table_cell">&nbsp;</div>
                        </div>

                        {% if users.items is defined %}
                            {% for user in users.items %}
                                <div class="table_row clickable-row" data-userow="{{ user.id }}" data-href='{{ url('users/show/' ~ user.id)  }}' >
                                    <div class="table_cell">
                                        <div class="btn-group dubble_dropdown sub_panel_dropdown">
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <div class="boxCheckbox userCheck" data-id="{{ user.id }}">
                                                    <div class="check">
                                                        <i class="fa fa-square-cube disabled_icon"></i>
                                                        <i class="fa fa-check-square ACTIVE_icon"></i>
                                                        <input type="checkbox" class="actualCheckItem" name="userId" value="{{ user.id }}" />
                                                    </div>
                                                </div>
                                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                                            </button>
                                            <div class="dropdown-menu screen_menu">
                                                <a class="dropdown-item resetUser" data-id="{{ user.id }}" >Reset password</a>
                                                <a class="dropdown-item removeUser red_color" data-id="{{ user.id }}" >Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table_cell">{{ user.name}} {{ user.surname }}</div>
                                    <div class="table_cell">{{ user.email }}</div>
                                    <div class="table_cell text-center">{{ user.gender }}</div>
                                    <div class="table_cell text-center">{{ user.birthday ? date('d.m.Y', strtotime(user.birthday)) : '' }}</div>
                                    <div class="table_cell">{{ user.city }}</div>
                                    <div class="table_cell">
                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    </div>
                                </div>
                            {% endfor  %}
                        {% else %}
                            <div class="table_row clickable-row" data-href='{{ url('users') }}' >
                                <div class="table_cell"></div>
                                <div class="table_cell">Users not found</div>
                                <div class="table_cell"></div>
                                <div class="table_cell"></div>
                                <div class="table_cell"></div>
                                <div class="table_cell"></div>
                                <div class="table_cell"></div>
                            </div>
                        {% endif %}

                    </div>
                </div>
            </div>
        </div>
    </form>

    {%  include "_partials/pagination"
    with [
        'paginator' : users,
        'page' : page,
        'path' : url('users/segment/' ~ segment.id, {
            'page' : ''
        })
    ]
    %}

    {% include '_modals/users/segment_edit.volt' %}
    {% include '_modals/users/user_delete.volt' %}
    {% include '_modals/reset_password.volt' %}
    {% include '_modals/message.volt' %}

{% endblock %}

{% block js %}
    {# JS #}
    {% include 'users/_users_scripts.volt' %}
    <script>
    	$(document).ready(function () {
            usersIndexScripts();   
            segmentScripts();         
        });
    </script>
{% endblock %}
